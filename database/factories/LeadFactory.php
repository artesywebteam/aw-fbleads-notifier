<?php

use Faker\Generator as Faker;

$factory->define(App\Lead::class, function (Faker $faker) {

    $form = factory(App\Form::class)->create();

    return [
        'fb_profile'    =>  function() use ($form){
            return $form->fb_profile;
        },
        'fb_form'   =>  function() use ($form){
            return $form->id;
        },
        'fb_id'     =>  $faker->randomNumber(6),
        'status_id'    =>  $faker->numberBetween(1,3),
    ];
});

$factory->state(App\Lead::class, 'data_in', function($faker){
    return [
        'data_in'   =>  json_encode([
            'full_name'             => $faker->name,
            'email'                 => $faker->email,
            'country'               => $faker->country,
            'phone_number'          => $faker->phoneNumber,
            'mensaje'               => $faker->text(),
            'follow_up_action_url'  => $faker->url,
        ])
    ];
});

$factory->state(App\Lead::class, 'data_out', function($faker){
    return [
        'data_out'   =>  json_encode([
            'nombre'        => $faker->name,
            'email'         => $faker->email,
            'pais'          => $faker->country,
            'telefono'      => $faker->phoneNumber,
            'mensaje'       => $faker->text(),
            'url'           => $faker->url
        ])
    ];
});