<?php

use Faker\Generator as Faker;

$factory->define(App\LeadSendingLog::class, function (Faker $faker) {
    return [
        'lead'      =>  function(){
            return factory(App\Lead::class)
                    ->states('data_in','data_out')
                    ->create()->id;
        },
        'status'    =>  strval($faker->randomNumber(3)),
        'reason'    =>  $faker->sentence(3),
    ];
});
