<?php

use Faker\Generator as Faker;

$factory->define(App\Fanpage::class, function (Faker $faker) {
    return [
        'fb_id'                 =>  $faker->randomNumber(6),
        'fb_name'               =>  $faker->name,
        'fb_access_token'       =>  $faker->sha256
    ];
});
