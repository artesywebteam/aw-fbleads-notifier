<?php

use Faker\Generator as Faker;

$factory->define(App\Form::class, function (Faker $faker) {
    return [
        'fb_profile'    =>  function(){
            return factory(App\Profile::class)->create()->id;
        },
        'fb_fanpage'    =>  function(){
            return factory(App\Fanpage::class)->create()->id;
        },
        'fb_id'         =>  $faker->randomNumber(6),
        'name'          =>  $faker->name,
    ];
});
