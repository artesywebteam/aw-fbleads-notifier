<?php

use Faker\Generator as Faker;

$factory->define(App\Profile::class, function (Faker $faker) {
    return [
        'fb_id'                 =>  $faker->randomNumber(6),
        'fb_name'               =>  $faker->name,
        'fb_access_token'       =>  $faker->sha256,
        'fb_token_expires_at'   =>  $faker->dateTimeBetween('now','+1 years')->getTimestamp(),
    ];
});
