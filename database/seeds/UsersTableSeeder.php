<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->updateOrInsert([
            'name'=>'pabloadi',
            'email'=>'artes@gmail.com',
            'password'=>bcrypt('artes12345')
        ]);
    }
}
