<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFormTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('form', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fb_profile');
            $table->unsignedInteger('fb_fanpage');
            $table->string('fb_id');
            $table->string('name');
            $table->timestamps();

            $table->foreign('fb_profile')->references('id')->on('profile')->onDelete('cascade');
            $table->foreign('fb_fanpage')->references('id')->on('fanpage')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('form');
        Schema::enableForeignKeyConstraints();
    }
}
