<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('fb_profile')->nullable();
            $table->unsignedInteger('fb_form')->nullable();
            $table->string('fb_id');
            $table->text('data_in');
            $table->text('data_out');
            $table->tinyInteger('status_id')->unsigned()->nullable();
            $table->timestamps();

            $table->foreign('fb_profile')->references('id')->on('profile')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('fb_form')->references('id')->on('form')->onDelete('set null')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('lead_status')->onDelete('set null')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::disableForeignKeyConstraints();
        Schema::dropIfExists('lead');
        Schema::enableForeignKeyConstraints();
    }
}
