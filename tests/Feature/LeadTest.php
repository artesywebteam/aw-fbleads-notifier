<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

class LeadTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    /**
     * Check index
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/lead');

        $response->assertStatus(200);
    }

    /**
     * Check ajax response from index
     *
     * @return void
     */
    public function testAjaxIndex()
    {
        $response = $this->get('/lead');

        $response->assertStatus(200);
    }

    /**
     * Check destroy a lead
     *
     * @return void
     */
    public function testDestroyLead()
    {
        $item = factory(\App\Lead::class)->states('data_in','data_out')->create();

        $response = $this->delete('/lead/'.$item->id);

        $response->assertOk();

        $result = \App\Lead::find($item->id);
        
        $this->assertTrue(is_null($result));
    }

    /**
     * Check not found fail destroy a lead
     *
     * @return void
     */
    public function testNotFoundDestroyLead()
    {
        $response = $this->delete('/lead/9999');

        $response->assertNotFound();

    }
}
