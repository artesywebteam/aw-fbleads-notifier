<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;

use Illuminate\Support\Facades\Crypt;

class FormTest extends TestCase
{
    use WithoutMiddleware;
    use RefreshDatabase;

    /**
     * Check index
     *
     * @return void
     */
    public function testIndex()
    {
        $response = $this->get('/form');

        $response->assertStatus(200);
    }

    /**
     * Check ajax response from index
     *
     * @return void
     */
    public function testAjaxIndex()
    {
        $response = $this->get('/form');

        $response->assertStatus(200);
    }

    /**
     * Check storing a form
     *
     * @return void
     */
    public function testStoreForm()
    {
        $form = factory(\App\Form::class)->make();
        $profile = factory(\App\Profile::class)->create();
        $fanpage = factory(\App\Fanpage::class)->make();


        $response = $this->post('/form',[
            'fb_profile'    =>  $profile->id,
            'fb_id'    =>  $form->fb_id,
            'name'    =>  $form->name,
            'fb_fanpage_id'    =>  $fanpage->fb_id,
            'fb_fanpage_name'    =>  $fanpage->fb_name,
            'fb_fanpage_access_token'    =>  Crypt::encryptString($fanpage->fb_access_token),
        ]);

        $response->assertStatus(201);

        $fanpage_result = \App\Fanpage::where('fb_id',$fanpage->fb_id)->first();
        $this->assertTrue(!is_null($fanpage_result), 'No fanpage were saved');

        $form_result = \App\Form::where('fb_id',$form->fb_id)->first();
        $this->assertTrue(!is_null($form_result), 'No form were saved');
    }

    /**
     * Check storing a empty data form
     *
     * @return void
     */
    public function testEmptyDataStoreForm()
    {
        $response = $this->post('/form',[]);

        $response->assertStatus(500);

    }

    /**
     * Check destroy a form
     *
     * @return void
     */
    public function testDestroyForm()
    {
        $form = factory(\App\Form::class)->create();

        $response = $this->delete('/form/'.$form->id);

        $response->assertOk();

        $result = \App\Form::where('fb_id',$form->fb_id)->first();

        $this->assertTrue(is_null($result));
    }

    /**
     * Check not found fail destroy a form
     *
     * @return void
     */
    public function testNotFoundDestroyForm()
    {
        $response = $this->delete('/form/9999');

        $response->assertNotFound();

    }
}
