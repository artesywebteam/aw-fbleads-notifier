<?php
namespace App\Amaw\Traits\Facebook;

use Crypt;
use App\Events\Error;

use App\Amaw\Traits\Facebook\FacebookClientTrait;
use Facebook\Authentication\AccessToken as FbAccessToken;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;

/**
 * 
 */
trait AccessTokenTrait
{
    use FacebookClientTrait;
    
    /**
     * Extend Facebook Access Token to Facebook AccessToken entity
     *
     * @param  string  $token
     * @return void
     */
    protected function extendFacebookAccessToken($token){

        $fb_instance = $fb_request = $fb_response = $fb_data = $extend_fb_token = null;

        try{
            $fb_instance = $this->getFacebookClient();
            $oauth = $fb_instance->getOAuth2Client();
            $extend_fb_token = $oauth->getLongLivedAccessToken($token);
            return $extend_fb_token;
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't get data to Facebook API");
        }

        throw new FacebookSDKException("Error at processing Extend Facebook Token", 1);

    }

    /**
     * Set string token to Facebook AccessToken entity
     *
     * @param  string  $token
     * @return string 
     */
    protected function getFacebookProfileName($token){

        if(is_string($token)){
            $token = new FbAccessToken($token);
        }

        $fb_instance = $fb_request = $fb_response = $fb_data = null;

        try {
            $fb_instance = $this->getFacebookClient();
            $fb_instance->setDefaultAccessToken($token);
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't connect to Facebook API");
        }

        try {
            $fb_response = $fb_instance->get('/me/');
            $fb_data = $fb_response->getDecodedBody();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't get data to Facebook API");
        }

        if($fb_data){
            if(isset($fb_data['name'])){
                return $fb_data['name'];
            }
        }

        throw new FacebookSDKException("Error at processing Facebook Profile name", 1);

    }

    /**
     * Remove app permissions from Facebook profile
     * 
     * @param App\Profile $profile
     * @return bool
     */
    protected function removeProfileAppPermissions(App\Profile $profile){
        abort_if(!$profile, 404);

        $fb_instance = $fb_request = $fb_response = $fb_data = null;

        try {
            $fb_instance = $this->getFacebookClient();
            $fb_instance->setDefaultAccessToken($profile->getAccessToken());
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't connect to Facebook API");
        }

        try {
            $fb_response = $fb_instance->delete('/me/permissions');
            $fb_data = $fb_response->getDecodedBody();
        } 
        catch (FacebookResponseException $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't get data to Facebook API");
        }
        catch (FacebookSDKException $e) {
            event(new ErrorEvent($e));
            abort(500, "Internal exception in Facebook");
        }

        if($fb_data){
            if(isset($fb_data['success'])){
                return (bool) $fb_data['success'];
            }
        }

        throw new FacebookSDKException("Error at removing app permissions of Profile", 1);

    }

    /**
     * Get access token object from encrypted token
     * 
     * @param string $encrypted_token
     * @return Facebook\Authentication\AccessToken
     */
    protected function getDecryptedAccessToken($encrypted_token){
        return new FbAccessToken(Crypt::decryptString($encrypted_token));
    }

    /**
     * Get encrypted access token string from access token object or string
     * 
     * @param string $token
     * @return string
     */
    protected function getEncryptedAccessToken($token){
        return Crypt::encryptString($token);
    }


}
