<?php
namespace App\Amaw\Traits\Facebook;

/**
 * 
 */
trait FacebookClientTrait
{
    /**
     * Get Facebook API client
     * 
     * @return Facebook\Facebook
     */
    public function getFacebookClient(){
        return resolve('fbapi');
    }

    
}
