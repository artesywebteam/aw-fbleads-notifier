<?php

namespace App;

use App\Events\LeadCreated;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Notifications\Notifiable;

class Lead extends Model
{
    use Notifiable;

    /**
     * name of table
     */
    protected $table = "lead";

    /**
     * Return a Profile model
    */
    public function profile(){
        return $this->hasOne('App\Profile','id','fb_profile');
    }

    /**
     * Return a Form model
    */
    public function form(){
        return $this->hasOne('App\Form','id','fb_form');
    }

    /**
     * Return a Fanpage model
    */
    public function fanpage(){
        return $this->hasOne('App\Fanpage','id','fb_fanpage');
    }

    /**
     * Return a Lead status model
    */
    public function status(){
        return $this->hasOne('App\LeadStatus','id','status_id');
    }

    /**
    * Get the token attribute (mutator)
    *
    * @param string
    * @return string
    */
    public function getDataInAttribute($value){
        return json_decode($value);
    }

    /**
    * Get the token attribute (mutator)
    *
    * @param string
    * @return string
    */
    public function setDataInAttribute($value){
        $this->attributes['data_in'] =  json_encode($value);
    }

    /**
    * Get the token attribute (mutator)
    *
    * @param string
    * @return string
    */
    public function getDataOutAttribute($value){
        return json_decode($value);
    }

    /**
    * Get the token attribute (mutator)
    *
    * @param string
    * @return string
    */
    public function setDataOutAttribute($value){
        $this->attributes['data_out'] =  json_encode($value);
    }

    

}
