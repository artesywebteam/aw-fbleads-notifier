<?php

namespace App\Broadcasting;

use Illuminate\Notifications\Notification;

class LeadNotificationDatabaseChannel
{
     /**
    *
    * @param mixed $notifiable
    * @param \Illuminate\Notifications\Notification  $notification
    * @return void
    */
    public function send($notifiable, Notification $notification){
        $data = $notification->toArray($notifiable);

        return $notifiable->routeNotificationFor('database')->create([
            'id'    =>  $notification->id,
            'type'    =>  get_class($notification),
            'notifiable_type'    =>  get_class($notifiable),
            'data'  =>  $data,
            'done'  =>  false,
            'read_at'   => null,
        ]);
    }
}
