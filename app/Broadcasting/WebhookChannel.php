<?php

namespace App\Broadcasting;

use Illuminate\Notifications\Notification;

use GuzzleHttp\Client as HttpClient;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\BadResponseException;

use Log;
use Exception;
use App\Notification as NotificationModel;

class WebhookChannel
{
    protected $client;
    protected $response;
    protected $code;
    protected $reason;

    public function __construct(){
        $this->client = new HttpClient();
    }

    /**
    *
    * @param mixed $notifiable
    * @param \Illuminate\Notifications\Notification  $notification
    * @return void
    */
    public function send($notifiable, Notification $notification){
        $processed_lead_data = json_decode($notifiable->data_out, true);
        $noti = NotificationModel::find($notification->id);
        

        Log::debug("webhook channel -> notification: ", [$notification]);
        Log::debug("webhook channel -> notification model: ", [$noti]);
        Log::debug("webhook channel -> processed lead data: ", [$processed_lead_data]);
        
        try {
            $this->response = $this->client->request('POST',config('endpoints.website'),[
                'form_params' => $processed_lead_data
            ]);

            if(!$this->response){
                throw new Exception('No response in endpoint');
            }
                
            $this->code = $this->response->getStatusCode();
            $this->reason = $this->response->getReasonPhrase();

            if($this->code != '200'){
                $this->saveLeadStatus($notifiable,3); //save error status
                Log::error("webhook channel -> error trying sending lead data to endpoint ");
            }
            else{
                $this->saveLeadStatus($notifiable,4); //save success status
                $noti->done = true;
                Log::debug("webhook channel -> success");
            }
        } catch (Exception $th) {
            $this->saveLeadStatus($notifiable,3); //save error status
            Log::error("webhook channel -> error trying sending lead data to endpoint ");
        }

        $noti->save();

        
    
    }

    /**
     * Save current lead status
     * 
     * @param App\Lead $lead
     * @param int $status
     * @return bool
     */
    protected function saveLeadStatus($lead, $status){
        try {
            $lead->status_id=$status;
            return $lead->save();
        } catch (\Throwable $th) {
            
        }
    }

    /**
     * Save result of current notification
     */
    protected function saveNotificationStatus($notification, $status){
        try {
            $notification->done = $status;
            $notification->save();
        } catch (\Throwable $th) {
            //throw $th;
        }
    }
}
