<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;
use FacebookAds\Api;

class FacebookAdsServiceProvider extends ServiceProvider implements DeferrableProvider
{

    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('fbads', function ($app) {

            $access_token = (string) $app['config']['facebook-ads.access_token'];

            Api::init(
                $app['config']['facebook-ads.app_id'],
                $app['config']['facebook-ads.app_secret'],
                $access_token
            );

            return Api::instance();

        });
    }

    /**
    * Get the services provided by the provider
    *
    * @return array
    */
    public function provides(){
        return  ['fbads'];
    }
}
