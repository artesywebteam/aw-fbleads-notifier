<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Support\DeferrableProvider;
use Facebook\Facebook;

class FacebookAPIServiceProvider extends ServiceProvider implements DeferrableProvider
{

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind('fbapi', function($app){
            return new Facebook([
                'app_id'                => config('facebook-ads.app_id'),
                'app_secret'            => config('facebook-ads.app_secret'),
                'default_graph_version' => config('facebook-ads.default_graph_version'),
                'http_client_handler'   => 'stream',
            ]);
        });
    }

    /**
    * Get the services provided by the provider
    *
    * @return array
    */
    public function provides(){
        return  ['fbapi'];
    }
}
