<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use DB;
use Log;
use Carbon\Carbon;
use App\Events\ErrorEvent;
use App\Events\LeadCreated;

use App\Fanpage;
use App\Lead;
use App\Form;

use GuzzleHttp\Client as HttpClient;
use GuzzleHttp\Exception\RequestException as HttpException;

use Facebook\Exceptions\FacebookResponseException;

class ProcessLeadJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $data;
    protected $fb_client;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($data = array())
    {
        $this->data = $data;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $data = $this->data;
        $saved_lead = null;

        try {
            $saved_lead = $this->saveLead($data, null);
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,'No se pudo guardar el prospecto en la base de datos');
        }
        
        Log::debug('Process lead job -> Facebook Lead leadgen_id',[strval($data['leadgen_id'])]);
        Log::debug('Process lead job -> Facebook Lead page_id',[strval($data['page_id'])]);

        $fl_data = null;

        $fanpage = Fanpage::where('fb_id',$data['page_id'])->firstOrFail();

        try{
            $this->fb_client = resolve('fbapi');
            $this->fb_client->setDefaultAccessToken($fanpage->getAccessToken());
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't connect to Facebook API");
        }

        try{

            $lf_response = $this->fb_client->get(sprintf("/%s",strval($data['leadgen_id'])));
            $lead_data_field = $lf_response->getDecodedBody();

            $fb_response = $this->fb_client->sendRequest('GET', sprintf("/%s",strval($data['form_id'])), ['fields'=>'follow_up_action_url']);
            $fl_data = $fb_response->getDecodedBody();

        } catch (FacebookResponseException $e) {
            if($e->getSubErrorCode() == '100'){
                $saved_lead->delete();
            }
            event(new ErrorEvent($e));
            abort(500,"Can't retrieve data from Facebook API");
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,"Can't retrieve data from Facebook API");
        }


        Log::debug('Process lead job -> Facebook Lead unhandled data',[$lead_data_field]);
        Log::debug('Process lead job -> Facebook Lead form unhandled data',[$fl_data]);

        $lead['url']  = $fl_data['follow_up_action_url'];
        $lead['l_lead_type'] = 2;
        $lead['l_lead_statu'] = 1;

        $field_data=[];

        foreach ($lead_data_field['field_data'] as $datalead) {
            $field_data[$datalead['name']] = $datalead['values'][0];
            switch ($datalead['name']) {
                case 'full_name':
                    $lead['nombre']=$datalead['values'][0];
                    break;
                case 'email':
                    $lead['email']=$datalead['values'][0];
                    break;
                case 'country':
                    $lead['pais']=$datalead['values'][0];
                    break;
                case 'phone_number':
                    $lead['telefono']=$datalead['values'][0];
                    break;
                case 'mensaje':
                    $lead['mensaje']=$datalead['values'][0];
                    break;
            }
        }

        $lead['l_form_content'] = json_encode($field_data);
        $lead['created_at'] = Carbon::parse($lead_data_field['created_time'])->toDateTimeString();

        Log::debug('Subscribe Facebook lead -> Facebook Lead Data about to save',[$lead]);

        try {
            $saved_lead->data_out = json_encode($lead);
            $saved_lead->save();

            event(new LeadCreated($saved_lead));
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,'No se pudo guardar el prospecto en la base de datos');
        }

        Log::info('Subscribe Facebook lead -> sending data done',['lead' => $lead]);
    }

    /**
    * Save lead data
    *
    * @param array $data_raw_lead 
    * @param array $processed_lead 
    * @return App\Lead
    */
    protected function saveLead($data_raw_lead, $processed_lead = null){
        $form = Form::where('fb_id',$data_raw_lead['form_id'])->first();

        $lead = new Lead();
        $lead->fb_form = $form->getKey();
        $lead->fb_profile = $form->fb_profile;
        $lead->fb_id = $data_raw_lead['leadgen_id'];
        $lead->data_in = json_encode($data_raw_lead);
        $lead->data_out = json_encode($processed_lead);
        $lead->status_id = 1; // waiting status

        if($lead->save())
            return $lead;

    }


}
