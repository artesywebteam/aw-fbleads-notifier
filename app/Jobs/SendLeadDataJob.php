<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Log;
use Exception;
use GuzzleHttp\Client as HttpClient;
use Guzzle\Http\Exception\CurlException;
use Guzzle\Http\Exception\BadResponseException;

use App\Lead;
use App\LeadSendingLog;

class SendLeadDataJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $lead;
    protected $response;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct(Lead $lead)
    {
        $this->lead = $lead;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        $processed_lead_data = json_decode($this->lead->data_out, true);

        Log::debug('processed lead data about to: ', $processed_lead_data);

        $client = new HttpClient();
        $this->response = $client->request('POST',config('endpoints.website'),[
            'form_params' => $processed_lead_data
        ]);

        $code = $this->response->getStatusCode();
        $reason = $this->response->getReasonPhrase();

        if($code != '200'){
            throw new Exception($reason, $code);
        }
        else{
            try {
                /**
                 * set sucess status to the lead
                 */
                $this->lead->status_id = 4; 
                $this->lead->save();
            } catch (Exception $th) {
                throw Exception('Failed to save success status of sending lead');
            }

            try {
                $this->logSendingLead($this->lead, $code, $reason);
            } catch (Exception $th) {
                throw Exception('Failed to save log of lead sending', $th->getCode(), $th->getMessage());
            }
            
        }
    }

    /**
     * The job failed to process.
     *
     * @return void
     */
    public function failed(Exception $exception){
        /**
         * set failed status to the lead
         */
        $this->lead->status_id = 3;
        $this->lead->save();

        try{
            if($this->response)
                $this->logSendingLead($this->lead, $this->response->getStatusCode(), $this->response->getReasonPhrase());
            else
                $this->logSendingLead($this->lead, $exception->getCode(), $exception->getMessage());
        } catch (Exception $th) {
            throw Exception('Failed to save log of lead sending', $th->getCode(), $th->getMessage());
        }
    }

    protected function logSendingLead(Lead $lead, $status, $reason){
        $log = new LeadSendingLog();
        $log->status = $status;
        $log->reason = $reason;
        $log->lead = $lead->id; 
        $log->save();
    }

}
