<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Notification extends Model
{
    protected $table = "notifications";

    protected $hidden = ['type','notifiable_type','data','read_at'];

    /**
     * Return a Profile model
    */
    public function lead(){
        return $this->hasOne('App\Lead','id','notifiable_id');
    }
}
