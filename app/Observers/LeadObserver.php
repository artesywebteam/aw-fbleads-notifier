<?php

namespace App\Observers;

use App\Lead;
use App\Notifications\WebhookLead;

class LeadObserver
{
    /**
     * Handle the lead "created" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function created(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "updated" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function updated(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "deleted" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function deleted(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "restored" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function restored(Lead $lead)
    {
        //
    }

    /**
     * Handle the lead "force deleted" event.
     *
     * @param  \App\Lead  $lead
     * @return void
     */
    public function forceDeleted(Lead $lead)
    {
        //
    }

    /**
    * Check the status of the lead and do a service according to it
    *
    * @param \App\Lead $lead
    * @return void
    */
    protected function checkStatus(Lead $lead){
        $result = null;
        switch ($lead->status) {
            case 1: // waiting status
                $lead->notify(new WebhookLead);
                break;

            default:
                # code...
                break;
        }

    }

}
