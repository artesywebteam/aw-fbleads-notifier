<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Form extends Model
{
    protected $table = "form";

    /**
     * Return a Profile model
    */
    public function profile(){
        return $this->hasOne('App\Profile','id','fb_profile');
    }

    /**
     * Return a Fanpage model
    */
    public function fanpage(){
        return $this->hasOne('App\Fanpage','id','fb_fanpage');
    }

}
