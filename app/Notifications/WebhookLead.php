<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Notifications\Notification;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;


use App\Broadcasting\WebhookChannel;
use App\Broadcasting\LeadNotificationDatabaseChannel;

class WebhookLead extends Notification
{
    use Queueable;

    /**
     * Create a new notification instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return [LeadNotificationDatabaseChannel::class, WebhookChannel::class];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            'data_in'   =>  $notifiable->data_in,
            'data_out'   =>  $notifiable->data_out,
        ];
    }

    public function toWebhook($notifiable){
        return $notifiable->data_out;
    }
}
