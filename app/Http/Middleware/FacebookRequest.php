<?php

namespace App\Http\Middleware;

use Closure;

class FacebookRequest
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->isMethod('get')){
            if(
                $request->has('hub_verify_token') &&
                $request->has('hub_mode') &&
                $request->input('hub_mode') === 'subscribe'
            ){
                if ($request->input('hub_verify_token') === config('facebook-ads.hub_verify_token')) {
                    return response($request->input('hub_challenge'),200);
                }
                else {
                    return response('Error on verify facebook realtime update subscribe',500);
                }
            }
        }

        return $next($request);
    }
}
