<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class ParseFacebookLeads
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has("entry")){
            Log::debug('Facebook input request has entry');

            $leads=$request->input("entry.*.changes.*.value");
            $request->replace([
                'leads' => $leads
            ]);

        }
        else{
            Log::debug('Facebook input request has not entry');
            abort(417, 'Facebook input request has not entry');
        }

        return $next($request);
    }
}
