<?php

namespace App\Http\Middleware;

use Closure;
use Log;

class VerifyFacebookXHubSignature
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->hasHeader('X-Hub-Signature')){
            $signature = explode('=',$request->header('X-Hub-Signature'));
            Log::debug('X-Hub-Signature',[$signature[1]]);
            $data = file_get_contents( "php://input" );
            Log::debug('X-Hub-Signature payload ',[$data]);
            $hash = hash_hmac($signature[0], $data, config('facebook-ads.app_secret'));
            Log::debug('Facebook Lead hash ',[$hash]);
            if($hash !== $signature[1]){
                return response('Error on matching X-Hub-Signature',500);
            }
        }

        return $next($request);
    }
}
