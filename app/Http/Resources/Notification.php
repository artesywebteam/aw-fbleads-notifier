<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class Notification extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'status'        =>  ($this->done) ? 'Éxito' : 'Fallido',
            'created_at'    =>  (string) $this->created_at,
        ];
    }
}
