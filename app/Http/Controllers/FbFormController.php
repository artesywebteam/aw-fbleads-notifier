<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Amaw\Traits\Facebook\AccessTokenTrait;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Contracts\Encryption\DecryptException;

class FbFormController extends Controller
{
    use AccessTokenTrait;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        try {
            $access_token = $this->getDecryptedAccessToken($id);
        } catch (DecryptException $e) {
            event(new ErrorEvent($e));
            abort(500,'No se pudo desencriptar el token de acceso');
        }

        if(!$access_token->isExpired()){
            $forms = [];
            $fb_instance = $fb_request = $fb_response = $fb_data = null;

            try {
                $fb_instance = $this->getFacebookClient();
                $fb_instance->setDefaultAccessToken($access_token);
                
            } catch (Exception $e) {
                event(new ErrorEvent($e));
                abort(500, "Can't connect to Facebook API");
            }

            try {
                $fb_response = $fb_instance->get('/me/leadgen_forms');
                $fb_data = $fb_response->getDecodedBody();
            } catch (Exception $e) {
                event(new ErrorEvent($e));
                abort(500, "Can't get data to Facebook API");
            }

            if($fb_data){
                return $fb_data['data'];
                foreach($fb_data['data'] as $key => $form){
                    $forms[]=[
                        'id'        => $form['id'],
                        'name'      => $form['name'],
                    ];
                }
            }

            return response()->json($forms);

        }
        else{
            abort(500, 'El token de acceso está expirado');
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
