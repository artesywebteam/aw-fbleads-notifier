<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;

use App\Form;
use App\Profile;
use App\Fanpage;

class FormController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $page = $request->input('page',1);
            $rowsPerPage = $request->input('rowsPerPage',20);
            $sort = $request->input('sortBy','created_at');
            $order = ($request->input('descending','false') === 'true') ? 'desc' : 'asc';

            $items = Form::with('profile','fanpage')
                     ->orderBy($sort,$order)
                     ->paginate($rowsPerPage,'*',null,$page);

            return response()->json($items);

        }

        return view('fbforms/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fanpage = null;

        try{
            $fanpage = $this->storeFanpage($request);
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "La página de Facebook no se pudo guardar");
        }

        try {
            $form = new Form();
            $form->fb_profile = $request->get('fb_profile');
            $form->fb_fanpage = $fanpage->getKey();
            $form->fb_id = $request->get('fb_id');
            $form->name = $request->get('name');
            $form->save();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "El formulario no se pudo guardar");
        }

        return response('ok',201);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $form = Form::findOrFail($id);

        try {
            $form->delete();

        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "El formulario no se pudo eliminar");
        }

        return response('ok',200);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \App\Fanpage
     */
    protected function storeFanpage(Request $request){
        if(!$request->has('fb_fanpage_access_token')){
            throw new Exception('No access token of the fanpage');
        }

        $access_token = $request->get('fb_fanpage_access_token');
        $fanpage_id = $request->get('fb_fanpage_id');
        $fanpage_name = $request->get('fb_fanpage_name');
        $fanpage_access_token = Crypt::decryptString($access_token);

        $fanpage = new Fanpage();
        $fanpage->fb_id = $fanpage_id;
        $fanpage->fb_name = $fanpage_name;
        $fanpage->fb_access_token = $fanpage_access_token;

        $fanpage->save();

        return $fanpage;

    }

}
