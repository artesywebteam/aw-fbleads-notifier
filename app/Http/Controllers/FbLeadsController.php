<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Jobs\ProcessLeadJob;

use Log;

class FbLeadsController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        try{
                $leads = $request->input('leads');

                foreach ($leads as $lead) {
                    $job = (new ProcessLeadJob($lead))->onQueue('fb_leads');
                    dispatch($job);
                }

                Log::debug('Jobs successful dispatched');

        }
        catch(Exception $e){
            event(new \App\Events\ErrorEvent($e));
            abort(500);
        }

        return response('ok',200);
    }


}
