<?php

namespace App\Http\Controllers\Lead;

use App\Lead;
use App\Events\LeadCreated;
use App\Events\ErrorEvent;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class SendingLeadController extends Controller
{
    /**
     * Sending the lead info to endpoint
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show(int $id)
    {
        $lead = Lead::findOrFail($id);
        
        try {
            event(new LeadCreated($lead)); 
        } catch (Exception $th) {
            event(new ErrorEvent($th));
            abort(500, 'No se pudo enviar datos del prospecto');
        }
        
        return response('Se ha enviado el prospecto correctamente',200);
    }
    
}
