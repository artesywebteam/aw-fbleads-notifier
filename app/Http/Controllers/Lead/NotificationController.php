<?php

namespace App\Http\Controllers\Lead;

use App\Lead;
use App\Notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Resources\NotificationCollection;

class NotificationController extends Controller
{
    /**
     * Display the specified resource.
     *
     * @param  \App\Lead  $lead
     * @return \Illuminate\Http\Response
     */
    public function show($lead)
    {
        return new NotificationCollection(
            Notification::where('notifiable_id',$lead)->get()
        );
    }
}
