<?php

namespace App\Http\Controllers\Lead;

use Log;
use App\Lead;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $page = $request->input('page',1);
            $rowsPerPage = $request->input('rowsPerPage',20);
            $sort = $request->input('sortBy','created_at');
            $order = ($request->input('descending','false') === 'true') ? 'desc' : 'asc';

            $leads = Lead::with('profile','form','status')
                     ->orderBy($sort,$order)
                     ->paginate($rowsPerPage,'*',null,$page);

            return response()->json($leads);
        }

        return view('leads/index');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $lead = Lead::findOrFail($id);

        try {
            $lead->status = $request->input('status');
            $lead->save();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, 'No se pudo actualizar el estado del prospecto');
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $lead
     * @return \Illuminate\Http\Response
     */
    public function destroy($lead)
    {
        $lead = Lead::findOrFail($lead);

        try {
            $lead->delete();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, 'No se pudo borrar el prospecto');
        }

        return response('El prospecto ha sido borrado correctamente',200);
    }


}
