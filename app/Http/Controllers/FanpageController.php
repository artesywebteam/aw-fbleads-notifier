<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Illuminate\Support\Facades\Crypt;
use App\Events\ErrorEvent;
use App\Profile;

class FanpageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $profile = Profile::findOrFail($id);

        $fbpages = [];

        try {
                if(!$profile->hasAccessToken()){
                    throw new Exception("There's no access token from profile", 1);
                }

        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,'No existe token de acceso del perfil');
        }

        $fb_instance = $fb_request = $fb_response = $fb_data = null;

        try {
            $fb_instance = resolve('fbapi');
            $fb_instance->setDefaultAccessToken($profile->getAccessToken());
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't connect to Facebook API");
        }

        try {
            $fb_response = $fb_instance->get('/me/accounts' );
            $fb_data = $fb_response->getDecodedBody();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500, "Can't get data to Facebook API");
        }

        if($fb_data){
            foreach ($fb_data['data'] as $key => $page) {
                $fbpages[]=[
                    'name'          => $page['name'],
                    'id'            => $page['id'],
                    'access_token'  => Crypt::encryptString($page['access_token']),
                ];
            }
        }

        return response()->json($fbpages);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
