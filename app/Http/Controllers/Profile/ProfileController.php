<?php

namespace App\Http\Controllers\Profile;

use App\Events\ErrorEvent;

use Log;
use App\Profile;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Amaw\Traits\Facebook\AccessTokenTrait;

use Illuminate\Support\Facades\Crypt;

class ProfileController extends Controller
{
    use AccessTokenTrait;
    
    /**
     * Display a listing of the resource.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->ajax()){
            $page = $request->input('page',1);
            $rowsPerPage = $request->input('rowsPerPage',20);
            $sort = $request->input('sortBy','fb_token_expires_at');
            $order = ($request->input('descending','false') === 'true') ? 'desc' : 'asc';

            $items = Profile::orderBy($sort,$order)
                     ->paginate($rowsPerPage,'*',null,$page);

            return response()->json($items);
        }

        return view('profiles/index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::where('fb_id',$id)->firstOrFail();

        try {
            $profile->delete();
            $this->removeProfileAppPermissions($profile->id);

            return response('ok',200);
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500);
        }



    }
}
