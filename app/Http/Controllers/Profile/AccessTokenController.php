<?php

namespace App\Http\Controllers\Profile;

use App\Events\ErrorEvent;

use App\Profile;
use App\Http\Controllers\Controller;
use App\Amaw\Traits\Facebook\AccessTokenTrait;

use Illuminate\Http\Request;


class AccessTokenController extends Controller
{
    use AccessTokenTrait;

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $fb_id = $request->get('id');
        $fb_access_token = $profile = null;

        if(Profile::where('fb_id',$fb_id)->exists()){
            return $this->update($request,$fb_id);
        }

        try{
            $fb_access_token = $this->extendFacebookAccessToken($request->get("access_token"));
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,'Error in extending Facebook Access token expiration time');
        }

        try {

            $profile = new Profile();

            $profile->fb_id = $fb_id;
            $profile->fb_name = $this->getFacebookProfileName($fb_access_token);
            $profile->fb_access_token = $fb_access_token->getValue();
            $profile->fb_token_expires_at = $fb_access_token->getExpiresAt()->getTimestamp();

            $profile->save();

        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500);
        }

        return response('Se ha logrado extender y guardar el token de acceso exitosamente',200);
    }

    /**
     * Update the access token
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  string  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id){
        $profile = Profile::where('fb_id',$id)->firstOrFail();
        $fb_access_token = $request->get("access_token");

        try {
            $fb_access_token = $this->extendFacebookAccessToken($fb_access_token);
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500,'Error in extending Facebook Access token expiration time');
        }

        try {
            $profile->fb_access_token = $fb_access_token->getValue();
            $profile->fb_token_expires_at = $fb_access_token->getExpiresAt()->getTimestamp();

            $profile->save();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500);
        }

        response('Se ha logrado actualizar el token de acceso',200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $profile = Profile::where('fb_id',$id)->firstOrFail();

        try {
            $profile->fb_access_token = "";

            $profile->save();
        } catch (Exception $e) {
            event(new ErrorEvent($e));
            abort(500);
        }

        response('Se ha logrado borrar el token de acceso',200);
    }

}
