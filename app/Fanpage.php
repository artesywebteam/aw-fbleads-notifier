<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Crypt;

use Facebook\Authentication\AccessToken as FbAccessToken;

class Fanpage extends Model
{
    protected $table = 'fanpage';
    protected $hidden = ['fb_access_token'];
    public $timestamps = false;

    /**
     * Get the token attribute (mutator)
     *
     * @param  string  $value
     * @return string
     */
    public function getFbAccessTokenAttribute($value){
        return Crypt::decryptString($value);
    }

    /**
     * set the token attribute (mutator)
     *
     * @param  string  $value
     * @return void
     */
    public function setFbAccessTokenAttribute($value){
         $this->attributes['fb_access_token'] = Crypt::encryptString($value);
    }


    public function hasAccessToken(){
        return ($this->fb_access_token) ? true : false;
    }

    public function getAccessToken(){
        return new FbAccessToken($this->fb_access_token);
    }
}
