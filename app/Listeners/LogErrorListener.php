<?php

namespace App\Listeners;

use App\Events\ErrorEvent;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use Log;

class LogErrorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  ErrorEvent  $event
     * @return void
     */
    public function handle(ErrorEvent $event)
    {
        $error_data = [
            'code' => $event->exception->getCode(),
            'file' => $event->exception->getFile(),
            'line' => $event->exception->getLine(),
            'message' => $event->exception->getMessage(),
            // 'trace' =>  $event->exception->getTraceAsString(),
        ];

        Log::error('An exception was raised -> ', $error_data);
    }
}
