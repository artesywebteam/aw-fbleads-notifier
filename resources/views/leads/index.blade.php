@extends('layouts.default')

@section('content')
    <div id="lead">
        <v-toolbar flat color="white">
            <v-toolbar-title>Prospectos</v-toolbar-title>
        </v-toolbar>

        <v-dialog
            v-model="dialog.show"
            scrollable 
            max-width="500px"
            transition="dialog-transition"
        >
            <v-card>
                <v-card-title primary-title>
                    Log de envíos
                </v-card-title>
                <v-card-text>
                    <v-data-table
                        
                        :total-items="logs.totalItems"
                        :headers="logs.headers"
                        :items="logs.data"
                        class="elevation-1"
                        :pagination.sync="logs.pagination"
                        item-key="id"
                        :loading="logs.loading"
                        :search="logs.search"
                    >
                        <v-progress-linear v-slot:progress color="blue" indeterminate></v-progress-linear>
                        <template v-slot:items="props">
                            <tr>
                                <td>
                                    @{{ props.item.status }}
                                </td>
                                <td>
                                    @{{ props.item.created_at }}
                                </td>
                                
                            </tr>
                        </template>
                    </v-data-table>
                </v-card-text>
            </v-card>   
        </v-dialog>

        <v-data-table
            v-model="table.selected"
            :items="table.items"
            :total-items="table.totalItems"
            :headers="table.headers"
            :pagination.sync="table.pagination"
            :expand="table.expand"
            :loading="table.loading"
            item-key="id"
        >
            <v-progress-linear v-slot:progress color="blue" indeterminate></v-progress-linear>
            <template v-slot:items="props">
                <tr is="lead" 
                    :lead="props.item"
                    v-on:show-snackbar="showSnackbar"
                    v-on:remove-lead="removeFromTable(props.item)"
                    v-on:show-lead-log="showLeadLogModal"
                    v-on:reload="getData"
                >
                </tr>
                
                
                
            </template>
            <template v-slot:expand="props">
                <v-card>
                    <v-card-text>
                        <v-layout row align-center justify-center fill-height>
                            <v-flex xs6>
                                <v-card >
                                    <v-card-title primary-title>
                                        Datos de entrada
                                    </v-card-title>
                                    <v-card-text>
                                        @{{ props.item.data_in }}
                                        
                                    </v-card-text>
                                </v-card>
                                
                            </v-flex>
                            <v-flex xs6>
                                <v-card >
                                    <v-card-title primary-title>
                                        Datos de salida
                                    </v-card-title>
                                    <v-card-text>
                                        
                                            @{{ props.item.data_out }}
                                        
                                    </v-card-text>
                                </v-card>
                            </v-flex>
                            
                        </v-layout>
                        
                        
                    </v-card-text>
                </v-card>
            </template>
        </v-data-table>
        
    </div>
    
@stop

@section('css')

    <link rel="stylesheet" href="{{ elixir('bundle/css/lead.css') }}">
@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/lead.js') }}"></script>
@stop
