@extends('layouts.default')

@section('content')
    <div id="fbform">
        <v-toolbar flat color="white">
            <v-toolbar-title>Formularios</v-toolbar-title>
            <v-spacer></v-spacer>
            <v-btn color="primary" v-on:click="dialog.show = true">
                Agregar formulario
                <v-icon right>add</v-icon>
            </v-btn>
        </v-toolbar>

        <v-data-table
            v-model="table.selected"
            :items="table.items"
            :total-items="table.totalItems"
            :headers="table.headers"
            :pagination.sync="table.pagination"
            :loading="table.loading"
            item-key="id"
        >
            <v-progress-linear v-slot:progress color="blue" indeterminate></v-progress-linear>
            <template v-slot:items="props">
                <tr 
                    is="fbform"
                    :fbform="props.item"
                    v-on:show-snackbar="showSnackbar"
                    v-on:remove-item="removeFromTable(props.item)"
                >
                </tr>
                
            </template>
            
        </v-data-table>

        <v-dialog
            v-model="dialog.show"
            scrollable
            max-width="500px"
            transition="dialog-transition"
        >
            <fbform-form
                v-if="dialog.show"
                @close-dialog="dialog.show = false"
                @new-item="getData()"
                action="create"
                :fbform="fbform_edit">
            </fbform-form>
        </v-dialog>
        

        <div class="">
            
        </div>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/fbform.js') }}"></script>
@stop
