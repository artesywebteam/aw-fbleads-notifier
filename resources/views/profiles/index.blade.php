@extends('layouts.default')

@section('content')
<div id="profile">
        <v-toolbar flat color="white">
            <v-toolbar-title>Perfiles</v-toolbar-title>
            <v-spacer></v-spacer>
            <facebook-button 
                v-on:show-snackbar="showSnackbar"
            >
                Agregar perfil
            </facebook-button>
        </v-toolbar>

        <v-data-table
            v-model="table.selected"
            :items="table.items"
            :total-items="table.totalItems"
            :headers="table.headers"
            :pagination.sync="table.pagination"
            :loading="table.loading"
            item-key="id"
        >
            <v-progress-linear v-slot:progress color="blue" indeterminate></v-progress-linear>
            <template v-slot:items="props">
                <tr 
                    is="profile"
                    :profile="props.item"
                    v-on:show-snackbar="showSnackbar"
                    v-on:remove-token="deleteToken"
                    v-on:remove-item="removeFromTable(props.item)"
                >
                </tr>
                
            </template>
            
        </v-data-table>
        
    </div>
@stop

@section('css')

    <link rel="stylesheet" href="{{ elixir('bundle/css/profile.css') }}">
@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/profile.js') }}"></script>
@stop
