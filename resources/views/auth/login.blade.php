@extends('layouts.base')

@section('body')

<v-app id="inspire">
    <v-content>
      <v-container fluid fill-height>
        <v-layout align-center justify-center>
          <v-flex xs12 sm8 md4>
            <v-card class="elevation-12">
              <v-toolbar dark color="primary">
                <v-toolbar-title>Acceso</v-toolbar-title>
              </v-toolbar>
              <v-card-text>
                  <v-form ref="loginform">
                  <v-text-field 
                    prepend-icon="person" 
                    name="login" 
                    label="Nombre usuario" 
                    type="text" 
                    v-model="username"
                    :rules="[rules.required, rules.min, rules.email]"></v-text-field>
                  <v-text-field 
                    id="password" 
                    prepend-icon="lock"
                    :append-icon="show_pass ? 'visibility' : 'visibility_off'"
                    name="password" 
                    label="Contraseña" 
                    :type="show_pass ? 'text' : 'password'" 
                    v-model="password"
                    :rules="[rules.required]"
                    v-on:click:append="show_pass = !show_pass"></v-text-field>
                </v-form>
              </v-card-text>
              <v-card-actions>
                <div v-if="error">
                  @{{ error }}
                </div>
                <v-spacer></v-spacer>
                <v-btn color="primary" :loading="loading" v-on:click="login()">Enviar</v-btn>
              </v-card-actions>
            </v-card>
          </v-flex>
        </v-layout>
      </v-container>
    </v-content>
  </v-app>

@endsection

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/login.js') }}"></script>
@stop