@extends('layouts.base')

@section('body')
<v-container>
    <v-card>
        <v-card-text>
            <h1 class="text-md-center">
                <strong>AVISO DE PRIVACIDAD SOBRE LA LEY DE PROTECCIÓN DE DATOS PERSONALES</strong>
            </h1>
            <br>
            <p>
                AMAW S.A.S., dando cumplimiento a la Ley 1581 de 2012 y al Decreto 1377 de 2013, se permite informar a sus clientes, proveedores, empleados y público en general, tanto activos como inactivos, que tengan vínculo directo e indirecto con la sociedad, Titulares de los Datos Personales que se encuentran incluidos en la base de datos de la compañía, la cual ha sido recolectada en desarrollo de su objeto social, con ocasión de las relaciones comerciales y laborales entabladas, lo siguiente:
            </p>

            <p>
                Los mecanismos a través de los cuales son almacenados estos datos son seguros y garantizan la confidencialidad de los mismos, debido a que se cuenta con los medios idóneos para impedir el acceso a éstos por parte de terceros no autorizados.
            </p>

            <p>
                Los Titulares de la información personal incluida en bases de datos, tienen los siguientes derechos respecto del tratamiento de la misma: (i) Conocer, actualizar y rectificar la información recogida; (ii) Solicitar, en cualquier momento, que su información sea rectificada, modificada, actualizada o eliminada; (iii) Solicitar prueba de la autorización emitida por el Titular, salvo lo previsto en la Ley 1581 de 2012; (iv) Ser informado, previa solicitud, respecto de los usos aplicados a sus datos personales; (v) Con el cumplimiento de los requisitos legales, presentar ante la Superintendencia de Industria y Comercio quejas por infracciones conforme a lo dispuesto en la Ley 1581 de 2012 y las demás normas que la modifiquen, adicionen o complementen; (vi) Revocar la autorización y/o solicitar la supresión de los datos y su administración, en casos de vulneración de los derechos y garantías constitucionales y legales; (vii) Acceder en forma gratuita a sus datos personales divulgados; y (viii) Exigir la confidencialidad y la reserva de la información suministrada.
            </p>

            <p>
                Para efectos de ejercer estos derechos, los Titulares de los datos personales se pueden contactar con la compañía, como responsable y/o encargado del tratamiento de la información, en el teléfono 311 799 3560 o en el correo electrónico: 
                <a href="mailto:andres@artesyweb.com">andres@artesyweb.com</a> 
            </p>

            <p>
                Se solicita atentamente a todos los Titulares de información que aparecen registrados en las bases de datos de la compañía, se sirvan autorizarla para continuar realizando el tratamiento de sus datos personales, los cuales han sido suministrados y se han incorporado en la base de datos y/o en documentos físicos y/o electrónicos de todo tipo con los que cuenta la compañía. 
            </p>

            <p>
                En caso que en un plazo de 30 días hábiles contados a partir de la presente publicación, el Titular de los datos personales que no haya manifestado expresamente que no autoriza el tratamiento de sus datos personales ni haya solicitado la supresión de los mismos de las bases de datos de la compañía, se entenderá que ha autorizado a la misma para continuar con el tratamiento de tales datos, de acuerdo a las políticas y procedimientos adoptados por la compañía para estos efectos, los cuales se pueden encontrar en la página web: 
                <a href="http://www.artesyweb.com/">http://www.artesyweb.com/</a>  o en las instalaciones de la compañía
            </p>

            <p>
                Cordialmente,
            </p>

            <p>
                <strong>
                <i>
                AMAW S.A.S <br>
                NIT: 900.665.917-7 <br>
                DIRECCIÓN: CARRERA 22a  # 12A-12 CALI - VALLE <br>
                CELULAR: 300 243 6677
                </i>
                </strong>
            </p>        
        </v-card-text>
        
    </v-card>    
</v-container>


@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/app.js') }}"></script>
@stop