@extends('layouts.default')

@section('content')
    <div class="" id="facebook-login">
        <facebook-button></facebook-button>
    </div>
@stop

@section('scripts')
    <script type="text/javascript" src="{{ elixir('bundle/js/facebook-login.js') }}"></script>
@stop
