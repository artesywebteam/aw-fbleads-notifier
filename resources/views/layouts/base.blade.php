<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, minimal-ui">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <title>
            Facebook Leads Notifier - @yield('title')
        </title>

        <!-- Styles -->
        <link rel="stylesheet" href="{{ elixir('bundle/css/vendor.css') }}">

        @yield('styles')
        @yield('css')
    </head>
    <body>
        <div id="app">
            @yield('body')
        </div>
        <!-- Scripts -->
        <script type="text/javascript" src="{{ elixir('bundle/js/manifest.js') }}"></script>
        <script type="text/javascript" src="{{ elixir('bundle/js/vendor.js') }}"></script>
        @yield('scripts')
        @stack("scripts")
    </body>
</html>