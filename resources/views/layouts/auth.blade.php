@extends('layouts/base')

@section('content')
    <div id="app">
        @yield('content')
    </div>
@endsection
