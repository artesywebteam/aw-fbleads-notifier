@extends('layouts/base')

@section('body')
        
            <v-app id="inspire">
                <v-navigation-drawer app permanent>
                    <v-list>
                        <!-- home -->
                        <v-list-tile href="/">
                            <v-list-tile-action>
                                <v-icon>dashboard</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>Inicio</v-list-tile-title>
                            </v-list-tile-content>    
                        </v-list-tile>
                        <!-- profile -->
                        <v-list-tile href="/profile">
                            <v-list-tile-action>
                                <v-icon>account_box</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>Perfiles</v-list-tile-title>
                            </v-list-tile-content>    
                        </v-list-tile>
                        <!-- forms -->
                        <v-list-tile href="/form">
                            <v-list-tile-action>
                                <v-icon>assignment</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>Formularios</v-list-tile-title>
                            </v-list-tile-content>    
                        </v-list-tile>
                        <!-- leads -->
                        <v-list-tile href="/lead">
                            <v-list-tile-action>
                                <v-icon>receipt</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>Prospectos</v-list-tile-title>
                            </v-list-tile-content>    
                        </v-list-tile>
                        <!-- logout -->
                        <v-list-tile href="{{ route('logout') }}" v-on:click.prevent="$refs.logoutform.submit()">
                            <v-list-tile-action>
                                <v-icon>exit_to_app</v-icon>
                            </v-list-tile-action>
                            <v-list-tile-content>
                                <v-list-tile-title>Salir</v-list-tile-title>
                            </v-list-tile-content>    
                        </v-list-tile>
                        
                    </v-list>
                </v-navigation-drawer>
            
                <v-content>
                    <v-container fluid>
                        
                        @yield('content')
                    </v-container>
                    <div id="snackbar">
                        <v-snackbar
                            v-if="snackbar"
                            v-model="snackbar.show"
                            :color="snackbar.color"
                            :multi-line="snackbar.multiline"
                        >
                            @{{ snackbar.text }}
                            <v-btn flat color="primary" @click.native="snackbar.show = false">Close</v-btn>
                        </v-snackbar>
                    </div>
                    <div id="dialog"></div>
                    <div id="logout">
                        <form ref="logoutform" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>
                    
                </v-content>
                <v-footer app>
                    
                </v-footer>
            </v-app>

@endsection