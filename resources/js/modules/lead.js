
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('lead', require('../components/Lead.vue'));

import snackbarMixin from '../mixins/snackbar';
import tableMixin from '../mixins/table';


const lead = new Vue({
    el: '#app',
    mixins: [snackbarMixin, tableMixin],
    data: {
        logs: {
            totalItems: 0,
            data: [],
            headers: [
                {
                    text: 'Estado',
                    align: 'left',
                    sortable: false,
                    value: 'status',
                },
                {
                    text: 'Fecha creación',
                    align: 'left',
                    sortable: true,
                    value: 'created_at',
                },
            ],
            pagination: {
                descending: true,
                sortBy: 'created_at'
            },
            loading: false,
            search: [],
        },
        dialog: {
            show: false
        },
        
        
    },
    mounted(){
        this.table.route = '/lead/';
        this.table.headers = [
            {
                text: 'Fecha creación',
                align: 'left',
                sortable: true,
                value: 'created_at',
            },
            {
                text: 'Formulario',
                align: 'left',
                sortable: false,
                value: 'form',
            },
            {
                text: 'Perfil',
                align: 'left',
                sortable: false,
                value: 'profile',
            },
            {
                text: 'Estado',
                align: 'left',
                sortable: false,
                value: 'status',
            },
            {
                text: 'Acciones',
                align: 'left',
                sortable: false,
                
            },
        ];
        this.getData();
    },
    methods: {
        showLeadLogModal(data){
            this.logs.data = data;
            this.dialog.show=true;
        }
    },
});
