
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('facebook-button', require('../components/FacebookButton.vue'));
Vue.component('profile', require('../components/Profile.vue'));

import snackbarMixin from '../mixins/snackbar';
import tableMixin from '../mixins/table';

const profile = new Vue({
    el: '#app',
    mixins: [snackbarMixin, tableMixin],
    data: {
        
    },
    mounted(){
        this.table.route = '/profile/';
        this.table.pagination.sortBy = 'fb_token_expires_at';
        this.table.pagination.descending = false;
        this.table.headers = [
            {
                text: 'ID',
                align: 'left',
                sortable: true,
                value: 'id',
            },
            {
                text: 'Perfil',
                align: 'left',
                sortable: false,
                value: 'fb_name',
            },
            {
                text: 'Fecha caducidad token',
                align: 'left',
                sortable: true,
                value: 'fb_token_expires_at',
            },
            {
                text: 'Acciones',
                align: 'left',
                sortable: false,
                
            },
        ];
        this.getData();
    },
    methods: {
        deleteToken(item){
            // TODO tag as tokenless profile
        },
        addFacebookProfile(){
            window.open('/access_token','__blank');
        },
        
    },
});