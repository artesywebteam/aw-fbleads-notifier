/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');


/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

 const auth = new Vue({
     el: "#app",
     data: {
        valid: true,
        username: "",
        password: "",
        rules: {
            required: value => !!value || 'Este campo es requerido',
            min: value => value.length >= 5 || 'Mínimo 5 caracteres',
            max: value => value.length <= 30 || 'Máximo 30 caracteres',
            email: value => {
                const pattern = /^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/
                return pattern.test(value) || 'Email inválido';
            },
        },
        loading: false,
        show_pass: false,
        error: ""
     },
     methods: {
        login(){
            if(this.$refs.loginform.validate()){
                this.loading = true;
                axios.post('/login',{
                    email: this.username,
                    password: this.password,
                })
                .then(() => window.location.assign("/home"))
                .catch((err) => this.error = err.response.data.message)
                .finally(() => this.loading = false)
                
            }
            
        }
     },
 })