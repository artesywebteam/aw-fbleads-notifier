
/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('../bootstrap');

/**
 * Next, we will create a fresh Vue application instance and attach it to
 * the page. Then, you may begin adding components to this application
 * or customize the JavaScript scaffolding to fit your unique needs.
 */

Vue.component('fbform', require('../components/FbForm.vue'));
Vue.component('fbform-form', require('../forms/fbform.vue'));

import snackbarMixin from '../mixins/snackbar';
import tableMixin from '../mixins/table';

const profile = new Vue({
    el: '#app',
    mixins: [snackbarMixin, tableMixin],
    data: {
        fbform_edit: {},
        dialog: {
            show: false
        },
    },
    mounted(){
        this.table.route = '/form/';
        this.table.headers = [
            {
                text: 'Fecha creación',
                align: 'left',
                sortable: true,
                value: 'created_at',
            },
            {
                text: 'Nombre',
                align: 'left',
                sortable: false,
                value: 'name',
            },
            {
                text: 'Perfil',
                align: 'left',
                sortable: false,
                value: 'profile.fb_name',
            },
            {
                text: 'Fanpage',
                align: 'left',
                sortable: false,
                value: 'fanpage.fb_name',
            },
            {
                text: 'Acciones',
                align: 'left',
                sortable: false,
                
            },
        ];
    },
    methods: {
        

    }
});
