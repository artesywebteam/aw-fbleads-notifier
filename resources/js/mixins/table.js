const tableMixin = {
    data: () => ({
        table: {
            route: '',
            items: [],
            totalItems: 0,
            selected: [],
            pagination: {
                descending: true,
                sortBy: 'created_at',
                rowsPerPage: 20
            },
            expand: false,
            loading: false,
            headers: []
        },
    }),
    methods: {
        getData(){
            this.table.loading = true;
            axios.get(this.table.route,{
                params: this.table.pagination
            })
            .then((response) => {
                this.table.items = response.data.data;
                this.table.totalItems = response.data.total;
            })
            .catch((err) => {
                let text = "No se  pudo cargar los items";
                if(this.snackbar){
                    this.error(text);
                }
                else{
                    console.log(text);
                }
                
            })
            .finally(() => this.table.loading = false );
        },
        removeFromTable(item){
            const index = this.table.items.indexOf(item);
            this.table.items.splice(index,1);
            this.table.totalItems -= 1;
        }
    },
    watch: {
        "table.pagination":{
            handler(){
                this.getData(this.table.route);
            },
            deep: true
        }
    },
}

export default tableMixin;