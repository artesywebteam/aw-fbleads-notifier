const snackbarMixin = {
    data: () => {
        return {
            snackbar: {
                show: false,
                color: '',
                mode: 'multiline',
                timeout: 6000,
                text: '',
            }
        }
    },
    methods: {
        showSnackbar(data){
            this.snackbar.color = data.color || this.snackbar.color;
            this.snackbar.text = data.text || this.snackbar.text;
            this.snackbar.show = true;
        },
        info(text){
            this.showSnackbar({
                color: 'info',
                text: text,
            });
        },
        error(text){
            this.showSnackbar({
                color: 'error',
                text: text,
            });
        },
        success(text){
            this.showSnackbar({
                color: 'success',
                text: text,
            });
        },
    }
};

export default snackbarMixin;