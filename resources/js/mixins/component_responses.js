const componentResponsesMixin = {
    methods: {
        successResponse(response){
            let text = (typeof response === 'string') ? response : response.data;
            this.$emit('show-snackbar',{
                color: 'success',
                text: text
            });
        },
        errorResponse(err){
            let text = (typeof err === 'string') ? err : err.message;
            this.$emit('show-snackbar',{
                color: 'error',
                text: text
            });
        }
    }
};

export default componentResponsesMixin;