const dialogMixin = {
    data: () => ({
        dialog: {
            show: false,
            title: '',
            text : '',
        },
    }),
    methods: {
        showDialog(data){
            this.dialog.title = data.title;
            this.dialog.text = data.text;
            this.dialog.show = true;
        },
    }
};

export default dialogMixin;