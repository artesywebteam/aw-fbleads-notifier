require('bootstrap-notify');

window.success = (message) => {
    return $.notify({
        message: message
    },
    {
        type: 'success'
    });
};

window.danger = (message) => {
    return $.notify({
        message: message
    },
    {
        type: 'danger'
    });
};

window.info = (message) => {
    return $.notify({
        message: message
    },
    {
        type: 'info'
    });
};
