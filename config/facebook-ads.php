<?php
return [
    'app_id'                =>  env('FB_APP_ID'),
    'app_secret'            =>  env('FB_APP_SECRET'),
    'access_token'          =>  env('FB_ACCESS_TOKEN'),
    'default_graph_version' =>  env('FB_GRAPH_VERSION'),
    'hub_verify_token'      =>  env('FB_APP_HUB_VERIFY_TOKEN'),
]
 ?>
