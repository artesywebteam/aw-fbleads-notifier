const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.setPublicPath('./public/bundle');

// js
mix
.js('resources/js/app.js', 'public/bundle/js')
.js('resources/js/modules/login.js', 'public/bundle/js')
.js('resources/js/modules/profile.js', 'public/bundle/js')
.js('resources/js/modules/fbform.js', 'public/bundle/js')
.js('resources/js/modules/lead.js', 'public/bundle/js')
.js('resources/js/modules/facebook-login.js', 'public/bundle/js');

// extract vendor libraries
mix.extract([
    'vue','axios','vuetify','moment'
]);

mix.
stylus('resources/stylus/vendor.styl', 'public/bundle/css')

// sass/css
mix
.sass('resources/sass/profile.scss', 'public/bundle/css')
.sass('resources/sass/lead.scss', 'public/bundle/css');

//copy


mix.version();
