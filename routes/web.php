<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::middleware(['auth'])->group(function(){
    Route::get('/', 'Lead\LeadController@index');
    Route::get('/home', 'Lead\LeadController@index')->name('home');

    Route::resource('profile', 'Profile\ProfileController');

    Route::resource('access_token', 'Profile\AccessTokenController');

    Route::resource('lead', 'Lead\LeadController');

    Route::resource('sendinglead', 'Lead\SendingLeadController');

    Route::resource('notification', 'Lead\NotificationController');

    Route::resource('form', 'FormController');

    Route::resource('fbform', 'FbFormController');

    Route::resource('fanpage', 'FanpageController');

});
